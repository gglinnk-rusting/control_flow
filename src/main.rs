#![allow(unused_parens)]
#![allow(unused)]

fn main() {
    let array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    let mut index = 0;
    let mut nbr = 3;
    let cmp = 5;

    while (nbr < cmp * 2) {
        nbr *= 2;
    }

    if (nbr < cmp) {
        println!("{} is > to {}", nbr, cmp);
    } else if (nbr == cmp) {
        println!("{} is = to {}", nbr, cmp);
    } else {
        println!("{} is > to {}", nbr, cmp);
    }

    while (index < 10) {
        println!("The value of element {} is : {}", index + 1, array[index]);
        index += 1;
    }

    index = 0;
    for elem in array.iter() {
        println!("The value of element {} is : {}", index + 1, elem);
        index += 1;
    }

    for number in (1..6).rev() {
        println!("{}!", number);
    }
}
